﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestRelativePosition : MonoBehaviour
{
    public Transform otherVector;

    private Vector2 otherPosition;

    [SerializeField] Vector2 myPosition;
    [SerializeField] Vector2 myOrientation;

    public float dot;
    public float angle;

    public float cosTeta;

    public void ShowAngle()
    {
        myPosition = transform.position;
        otherPosition = otherVector.position;


        // Just for debugging
        dot = Vector2.Dot(myPosition, otherPosition);
        angle = Vector2.Angle(myPosition, otherPosition);

        cosTeta = Vector2.Dot(myOrientation.normalized, (otherPosition - myPosition));

        if (cosTeta > 0)
            Debug.Log("Is in front");
        else
            Debug.Log("Is behinde");
    }

    private void Update()
    {
        ShowAngle();
    }
}
