﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PresurePlate : MonoBehaviour
{
    [SerializeField]
    private Puzzle puzzle;

    [ContextMenu("Test")]
    private void OnPressed()
    {
        puzzle?.OnPlatePressed(this);
    }
}
