﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle : MonoBehaviour
{
    [SerializeField]
    private int currentPuzzleIndex = 0;
    
    [SerializeField]
    private PresurePlate[] plates = new PresurePlate[0];

    public void OnPlatePressed(PresurePlate pressedPlate)
    {
        if (ComparePlatformIndex(GetPlateformIndex(pressedPlate), currentPuzzleIndex))
        {
            NextIndex();
        }
        else
            ResetIndex();
    }

    private void NextIndex()
    {
        Debug.Log("Next");
        currentPuzzleIndex++;
    }

    private void ResetIndex()
    {
        Debug.Log("Reset");
        currentPuzzleIndex = 0;
    }

    private bool ComparePlatformIndex(int[] indexes, int currenIndex)
    {
        for (int i = 0; i < indexes.Length; i++)
        {
            if (indexes[i] == currenIndex)
            {
                return true;
            }
        }
        return false;
    }

    private int[] GetPlateformIndex(PresurePlate pressedPlate)
    {
        List<int> auxIndexs = new List<int>();
        int[] indexes;

        for (int i = 0; i < plates.Length; i++)
        {
            if (plates[i] == pressedPlate)
            {
                auxIndexs.Add(i);
            }
        }

        indexes = new int[auxIndexs.Count];

        for (int i = 0; i < indexes.Length; i++)
        {
            indexes[i] = auxIndexs[i];
        }

        return indexes;
    }

}
