﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pyros;

[RequireComponent(typeof(RotationTween))]
[RequireComponent(typeof(Movement))]
public class CubeBehaviour : MonoBehaviour
{
    [SerializeField] private float _timeThreshold = 0.5f;

    [SerializeField] private float _yDisplacementAmount;
    [SerializeField] private float _outerSphereRadius = 0f;
    [SerializeField] private float _innerSphereRadius = 0f;

    [SerializeField] Movement _movement;
    [SerializeField] Movement _yMovement;
    [SerializeField] Movement _jumpMovement;
    RotationTween _rotation;

    Vector3[] auxPositions = new Vector3[3];
    Vector3[] _yPositionsDisplacement= new Vector3[2];

    [SerializeField] private bool _canMove = true;

    private void Awake()
    {
        _rotation = GetComponent<RotationTween>();

        _outerSphereRadius = Mathf.Sqrt((Mathf.Pow(this.transform.localScale.x, 2) + Mathf.Pow(this.transform.localScale.x, 2))) / 2;
        _innerSphereRadius = this.transform.localScale.x /2;

        _yDisplacementAmount = _outerSphereRadius - _innerSphereRadius;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            Debug.Log("oli");
            if (_canMove)
            {
                auxPositions[0] = this.transform.position;
                auxPositions[1] = this.transform.position + (Vector3.forward / 2);
                auxPositions[2] = this.transform.position + Vector3.forward;

                _yPositionsDisplacement[0] = new Vector3(auxPositions[0].x, auxPositions[0].y, auxPositions[0].z);
                _yPositionsDisplacement[1] = new Vector3(auxPositions[1].x, auxPositions[1].y + _yDisplacementAmount, auxPositions[1].z);

                _movement.StartMove(auxPositions, _timeThreshold ,ActivateMovement);
                _yMovement.StartMove(_yPositionsDisplacement, _timeThreshold /2, ActivateMovement);

                _rotation.RotateObject(this.gameObject, Vector3.right, 90);
            }
            _canMove = false;
        }
        if (Input.GetKey(KeyCode.S))
        {
            if (_canMove)
            {
                auxPositions[0] = this.transform.position;
                auxPositions[1] = this.transform.position - (Vector3.forward / 2);
                auxPositions[2] = this.transform.position - Vector3.forward;

                _yPositionsDisplacement[0] = new Vector3(auxPositions[0].x, auxPositions[0].y, auxPositions[0].z);
                _yPositionsDisplacement[1] = new Vector3(auxPositions[1].x, auxPositions[1].y + _yDisplacementAmount, auxPositions[1].z);

                _movement.StartMove(auxPositions, _timeThreshold, ActivateMovement);
                _yMovement.StartMove(_yPositionsDisplacement, _timeThreshold / 2, ActivateMovement);

                _rotation.RotateObject(this.gameObject, -Vector3.right, 90);
            }
            _canMove = false;
        }
        if (Input.GetKey(KeyCode.A))
        {
            if (_canMove)
            {
                auxPositions[0] = this.transform.position;
                auxPositions[1] = this.transform.position - (Vector3.right / 2);
                auxPositions[2] = this.transform.position - Vector3.right;

                _yPositionsDisplacement[0] = new Vector3(auxPositions[0].x, auxPositions[0].y, auxPositions[0].z);
                _yPositionsDisplacement[1] = new Vector3(auxPositions[1].x, auxPositions[1].y + _yDisplacementAmount, auxPositions[1].z);

                _movement.StartMove(auxPositions, _timeThreshold, ActivateMovement);
                _yMovement.StartMove(_yPositionsDisplacement, _timeThreshold / 2, ActivateMovement);

                _rotation.RotateObject(this.gameObject, Vector3.forward, 90);
            }
            _canMove = false;
        }
        if (Input.GetKey(KeyCode.D))
        {
            if (_canMove)
            {
                auxPositions[0] = this.transform.position;
                auxPositions[1] = this.transform.position + (Vector3.right / 2);
                auxPositions[2] = this.transform.position + Vector3.right;

                _yPositionsDisplacement[0] = new Vector3(auxPositions[0].x, auxPositions[0].y, auxPositions[0].z);
                _yPositionsDisplacement[1] = new Vector3(auxPositions[1].x, auxPositions[1].y + _yDisplacementAmount, auxPositions[1].z);
                
                _movement.StartMove(auxPositions, _timeThreshold, ActivateMovement);
                _yMovement.StartMove(_yPositionsDisplacement, _timeThreshold / 2, ActivateMovement);

                _rotation.RotateObject(this.gameObject, -Vector3.forward, 90);
            }
            _canMove = false;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (_canMove)
            {
                _canMove = false;
         
                _yPositionsDisplacement[0] = new Vector3(this.transform.position.x, auxPositions[0].y, this.transform.position.z);
                _yPositionsDisplacement[1] = new Vector3(this.transform.position.x, auxPositions[1].y + 1, this.transform.position.z);

                _jumpMovement.StartMove(_yPositionsDisplacement, _timeThreshold * 0.8f, ActivateMovement);
            }
        }
    }

    public void ActivateMovement()
    {
        _canMove = true;
    }

}
