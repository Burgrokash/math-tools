﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour
{
    /// <summary>
    /// Is it dead?
    /// </summary>
    public bool IsDead
    {
        get
        {
            return _isDead;
        }
        set
        {
            _isDead = value;

            if (_isDead)
                Die();
        }
    }
    [SerializeField]
    private bool _isDead;

    /// <summary>
    /// The current amount of Health.
    /// </summary>
    public float Health
    {
        get
        {
            return _health;     
        }
        set
        {
            _health = value;

            if (_health <= 0)
                IsDead = true;
        }
    }
    [SerializeField]
    private float _health;

    /// <summary>
    /// The maximum health that the destructible can have in this moment.
    /// </summary>
    private float maxHealth;

    /// <summary>
    /// The initial health when first appears.
    /// </summary>
    private float initialHealth;

    /// <summary>
    /// Called when the destructible is Dead.
    /// </summary>
    private void Die()
    {
        Debug.Log("OMG I'm dead!!!");
        this.gameObject.SetActive(false);
    }

    /// <summary>
    /// Deals an amount of damage to the destructible.
    /// </summary>
    /// <param name="dealer">The object that has dealt the damage, is useful to know it if you want to send a callback, or do something to it.</param>
    /// <param name="amountOfDamage">The amount of damage received.</param>
    public void Hit(DamageDealer dealer, float amountOfDamage)
    {
        /*
         * TODO:
         * Estaria bueno poder mandarle un delegate para hacer un CallBack, ya se si se murio o si recibio daño o lo que sea.
         */ 

        Health -= amountOfDamage;
    }

}
