﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDealer: MonoBehaviour 
{
    /// <summary>
    /// The total raw damage that it can deal.
    /// </summary>
    [SerializeField]
    private float _damage;

    /// <summary>
    /// The current objective. Without it, its imposible to deal any damage.
    /// </summary>
    [SerializeField]
    private Destructible _currentObjective;

    public void DealDamage()
    {
        /*
         * TODO:
         * Puede ser que tengas algun componente que te bustea entonces en ese caso
         * estaria bueno poder llamarlo primero para que te aumente el daño, y despues
         * con eso llamar el Hit del destructible.
         */ 

        if (_currentObjective == null)
        {
            Debug.LogWarning("You have no objective to hit.");
            return;
        }
        else
        {
            _currentObjective.Hit(this, _damage);
        }
    }

}