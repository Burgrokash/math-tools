﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Pool))]
public class DynamicGrid : MonoBehaviour
{
    /// <summary>
    /// All the entities contained within the grid.
    /// </summary>
    public List<GridEntity> entities;

    [Header("Grid Settings")]
    /// <summary>
    /// The x and y offSet of the Tiles
    /// </summary>
    [SerializeField]
    [Tooltip("The X and Y offSet of the Tiles")]
    private Vector2 offSet;

    /// <summary>
    /// The gameObject used for tiling. 
    /// </summary>
    private Pool tileObjectToUsed;

    /// <summary>
    /// The dimensions expresed in ZxZ of the matrix. 
    /// </summary>
    [SerializeField]
    private Vector2Int size;

    [Space]
    public List<DynamicTile> matrix;

    public List<Vector2Int> existingTilesOnTheMatrix;

    private void Awake()
    {
        //if (!GetComponent<Pool>().ObjectToPool.GetComponent<Tile>())
          //  Debug.Log("The object you are using dont have a Tile on it.");

        tileObjectToUsed = GetComponent<Pool>();

        InitializeMatrix(size);

    }

    public void InitializeMatrix(Vector2Int _size)
    {
        matrix.Clear();

        for (int i = 0; i < _size.x; i++)
            for (int j = 0; j < _size.y; j++)
                CreateTile(i, j);
    }

    public void CreateTile(int x, int y)
    {
        DynamicTile pooledTile;

        pooledTile = tileObjectToUsed.GetPooledObject().GetComponent<DynamicTile>();

        pooledTile.SetTile(x, y, offSet);
        pooledTile.transform.position = new Vector2(pooledTile.xyPosition.x, pooledTile.xyPosition.y);

        matrix.Add(pooledTile);
        existingTilesOnTheMatrix.Add(new Vector2Int(x, y));

        pooledTile.CheckForAdjacencies(ref matrix);
    }
   
    public void DestroyTile(DynamicTile tileToDestroy)
    {
        
    }

    /// <summary>
    /// Returns a desired tile with x and y coordinates, if doesn't exist then returns null.
    /// </summary>
    /// <param name="xIndex">The x coordinate of the desired tile.</param>
    /// <param name="yIndex">The y coordinate of the desired tile.</param>
    /// <returns></returns>
    public DynamicTile GetTile(int xIndex, int yIndex)
    {
        foreach (DynamicTile tile in matrix)
            if (tile.coordinates.x == xIndex && tile.coordinates.y == yIndex)
                return tile;

        Debug.Log("The Tile don't exist");
        return null;

    }

    public DynamicTile GetTile(int _Id)
    {
        foreach (DynamicTile tile in matrix)
            if (tile.id == _Id)
                return tile;

        Debug.Log("The Tile don't exist");
        return null;
    }

    [ContextMenu("New Entity")]
    public void CreateEntity(Tile tileOnWichPut)
    {
        // Esto despues hayu que hacerlo con pools pero bue.
        GridEntity newEntity;
        newEntity = Instantiate(entities[0]);
        newEntity.myActualTile = tileOnWichPut;
        tileOnWichPut.AddEntity(newEntity);
    }

}
