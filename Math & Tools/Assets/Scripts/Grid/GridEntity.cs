﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridEntity : MonoBehaviour
{
    public float hight;

    [SerializeField]
    private new string name;

    private int id;

    public Tile myActualTile;

    public void SetActualTile(Tile myNewTile)
    {
        myActualTile = myNewTile;

        // Otra opcion es hacer esto en el manejador de inputs.
        myNewTile.AddEntity(this);
    }

}
