﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Proto class that has the general properties and methods to manage Grid Entities.
/// </summary>
public class GridEntitesManager : MonoBehaviour
{
    public Pool gridEntities;

    /// <summary>
    /// All the entities contained within the grid.
    /// </summary>
    public List<GridEntity> entitiesOnTheGrid;

    private void Awake()
    {
        gridEntities = GetComponent<Pool>();
    }

    public void CreateEntity(Tile tileOnWichPut)
    {
        Debug.Log("Tile is: " + tileOnWichPut.name);
        GridEntity newGridEntity = gridEntities.GetPooledObject().GetComponent<GridEntity>();
        Debug.Log("NewGridEntity: " + newGridEntity.name);
        AddEntity(newGridEntity);
        newGridEntity.GetComponent<GridMovement>().SetEntityPosition(newGridEntity, tileOnWichPut);
        newGridEntity.SetActualTile(tileOnWichPut);
    }

    private GridEntity GetGridEntity()
    {
        return gridEntities.GetPooledObject().GetComponent<GridEntity>();
    }

    private void AddEntity(GridEntity entityToAdd)
    {
        entitiesOnTheGrid.Add(entityToAdd);
    }

    private void RemoveEntity(GridEntity entityToRemove)
    {
        entitiesOnTheGrid.Remove(entityToRemove);
    }

}
