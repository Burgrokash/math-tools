﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class EneabledForAttribute : PropertyAttribute
{
    public string propertyName;

    public int Enum;

    public Types types;

    public enum Types
    {
        Bool,
        Enum,
    }

    public EneabledForAttribute(string propertyName)
    {
        this.propertyName = propertyName;
        this.types = Types.Bool;
    }

    public EneabledForAttribute(string propertyName, int Enum)
    {
        this.propertyName = propertyName;
        this.Enum = Enum;
        this.types = Types.Enum;
    }
}

[CustomPropertyDrawer(typeof(EneabledForAttribute))]
public class EneabledForDrawer : PropertyDrawer
{
    bool enabled = true;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        if (enabled)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }

        return -EditorGUIUtility.standardVerticalSpacing;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EneabledForAttribute myAttribute = (EneabledForAttribute)attribute;

        switch (myAttribute.types)
        {
            case EneabledForAttribute.Types.Bool:
                if (property.serializedObject.FindProperty(myAttribute.propertyName) == null)
                    Debug.LogWarning("Estas escribiendo mal la variable: " + myAttribute.propertyName);
                else
                    enabled = property.serializedObject.FindProperty(myAttribute.propertyName).boolValue;
                break;
            case EneabledForAttribute.Types.Enum:
                if (property.serializedObject.FindProperty(myAttribute.propertyName) == null)
                    Debug.LogWarning("Estas escribiendo mal la variable: " + myAttribute.propertyName);
                else
                    enabled = property.serializedObject.FindProperty(myAttribute.propertyName).enumValueIndex == myAttribute.Enum;
                break;
        }

        if (enabled)
        {
            EditorGUI.PropertyField(position, property, label, true);
        }
    }
}