﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DisableForAttribute : PropertyAttribute
{
    public string propertyName;

    public int Enum;

    public Types types;

    public enum Types
    {
        Bool,
        Enum,
    }

    public DisableForAttribute(string propertyName)
    {
        this.propertyName = propertyName;
        this.types = Types.Bool;
    }

    public DisableForAttribute(string propertyName, int Enum)
    {
        this.propertyName = propertyName;
        this.Enum = Enum;
        this.types = Types.Enum;
    }
}

[CustomPropertyDrawer(typeof(DisableForAttribute))]
public class DisableForDrawer : PropertyDrawer
{
    bool enabled;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        if (enabled)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }

        return -EditorGUIUtility.standardVerticalSpacing;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        DisableForAttribute myAttribute = (DisableForAttribute)attribute;

        switch (myAttribute.types)
        {
            case DisableForAttribute.Types.Bool:
                enabled = !property.serializedObject.FindProperty(myAttribute.propertyName).boolValue;
                break;
            case DisableForAttribute.Types.Enum:
                enabled = property.serializedObject.FindProperty(myAttribute.propertyName).enumValueIndex != myAttribute.Enum;
                break;
        }

        if (enabled)
        {
            EditorGUI.PropertyField(position, property, label, true);
        }
    }
}