﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Movement : MonoBehaviour
{
    public EasingFunction.Ease type;
    private EasingFunction.Function function;

    [SerializeField]
    private bool loop;

    [SerializeField]
    private bool rewindOnEnd;

    [SerializeField]
    private float elapsedTime;

    [SerializeField]
    private float movementDuration;

    [SerializeField]
    private Transform[] positions;
    private Vector3[] auxPositions;

    [Header("Clamp")]
    [SerializeField]
    private bool xAxis;

    [SerializeField]
    private bool yAxis;

    [SerializeField]
    private bool zAxis;

    [Space]

    public UnityEvent OnStart;
    public UnityEvent OnNewPosition;
    public UnityEvent OnEnd;
    public UnityAction action;

    private Coroutine _movementCoroutine;

    [Space]
   [SerializeField] bool invertFunctionOnRewind = false;

    private void OnValidate()
    {
        auxPositions = new Vector3[positions.Length];

        for (int i = 0; i < positions.Length; i++)
            auxPositions[i] = positions[i].position;
    }

    [ContextMenu("StartMoving")]
    public void TestMove()
    {
        StartMove(null);
    }

    public void StartMove(Action callBack)
    {
        if (_movementCoroutine != null)
            StopCoroutine(_movementCoroutine);
        
        _movementCoroutine = StartCoroutine(Move(auxPositions, movementDuration, callBack));
    }

    public void StartMove(float duration, Action callBack)
    {
        if (_movementCoroutine != null)
            StopCoroutine(_movementCoroutine);

        _movementCoroutine = StartCoroutine(Move(auxPositions, duration, callBack));
    }

    public void StartMove(Vector3[] positions, Action callBack)
    {
        if (_movementCoroutine != null)
            StopCoroutine(_movementCoroutine);

        _movementCoroutine = StartCoroutine(Move(positions, movementDuration, callBack));
    }

    public void StartMove(Vector3[] positions, float duration, Action callBack)
    {
        if (_movementCoroutine != null)
            StopCoroutine(_movementCoroutine);

        _movementCoroutine = StartCoroutine(Move(positions, duration, callBack));
    }


    IEnumerator Move(Vector3[] positions, float duration, Action callBack)
    {
        action += Messange;
        OnStart.AddListener(action);

        OnStart.Invoke();

        function = EasingFunction.GetEasingFunction(type);

        elapsedTime = 0f;

        float x, y, z, distancePercentage, durationPercentage;
        float totalDistance = GetTotalDistance(positions);

        do
        {
            OnStart.Invoke();

            transform.position = positions[0];

            for (int i = 0; i < positions.Length - 1; i++)
            {
                x = positions[i].x;
                y = positions[i].y;
                z = positions[i].z;

                distancePercentage = Vector3.Distance(positions[i], positions[i + 1]) / totalDistance;
                durationPercentage = duration * distancePercentage;

                while (elapsedTime < durationPercentage)
                {
                    x = function(positions[i].x, positions[i + 1].x, (elapsedTime / durationPercentage));
                    y = function(positions[i].y, positions[i + 1].y, (elapsedTime / durationPercentage));
                    z = function(positions[i].z, positions[i + 1].z, (elapsedTime / durationPercentage));

                    elapsedTime += Time.deltaTime;

                    if (xAxis)
                        x = transform.position.x;
                    if (yAxis)
                        y = transform.position.y;
                    if (zAxis)
                        z = transform.position.z;

                    transform.position = new Vector3(x, y, z);

                    yield return null;
                }

                if (!xAxis && !yAxis && !zAxis)
                    transform.position = positions[i + 1];

                elapsedTime = 0f;

                if (i < positions.Length - 2)
                    OnNewPosition.Invoke();
            }

            if (!rewindOnEnd)
            {
                OnEnd.Invoke();
                callBack?.Invoke();
            }
            else
            {
                if (invertFunctionOnRewind)
                {
                    InvertFunction();
                }
                for (int i = positions.Length - 1 ; i > 0; i--)
                {
                    x = positions[i].x;
                    y = positions[i].y;
                    z = positions[i].z;

                    distancePercentage = Vector3.Distance(positions[i], positions[i - 1]) / totalDistance;
                    durationPercentage = duration * distancePercentage;

                    while (elapsedTime < durationPercentage)
                    {
                        x = function(positions[i].x, positions[i - 1].x, (elapsedTime / durationPercentage));
                        y = function(positions[i].y, positions[i - 1].y, (elapsedTime / durationPercentage));
                        z = function(positions[i].z, positions[i - 1].z, (elapsedTime / durationPercentage));

                        elapsedTime += Time.deltaTime;

                        if (xAxis)
                            x = transform.position.x;
                        if (yAxis)
                            y = transform.position.y;
                        if (zAxis)
                            z = transform.position.z;

                        transform.position = new Vector3(x, y, z);

                        yield return null;
                    }

                    if (!xAxis && !yAxis && !zAxis)
                        transform.position = positions[i - 1];

                    elapsedTime = 0f;

                    if (i > positions.Length + 2)
                        OnNewPosition.Invoke();
                }

                OnEnd.Invoke();
                callBack?.Invoke();

            }
        }
        while (loop);
    }

    public void Messange()
    {
        Mathf.Abs(5f);
    }

    public float GetTotalDistance(Vector3[] positions)
    {
        float totalDistance = 0;

        for (int i = 0; i < positions.Length - 1; i++)
            totalDistance += Vector3.Distance(positions[i], positions[i + 1]);

        return totalDistance;
    }

    private void InvertFunction()
    {
        switch (type)
        {
            case EasingFunction.Ease.EaseInQuad:
                function = EasingFunction.GetEasingFunction(EasingFunction.Ease.EaseOutQuad);
                break;
            case EasingFunction.Ease.EaseOutQuad:
                function = EasingFunction.GetEasingFunction(EasingFunction.Ease.EaseInQuad);
                break;
            case EasingFunction.Ease.EaseInOutQuad:
                break;
            case EasingFunction.Ease.EaseInCubic:
                function = EasingFunction.GetEasingFunction(EasingFunction.Ease.EaseOutCubic);
                break;
            case EasingFunction.Ease.EaseOutCubic:
                function = EasingFunction.GetEasingFunction(EasingFunction.Ease.EaseInCubic);
                break;
            case EasingFunction.Ease.EaseInOutCubic:
                break;
            case EasingFunction.Ease.EaseInQuart:
                function = EasingFunction.GetEasingFunction(EasingFunction.Ease.EaseOutQuart);
                break;
            case EasingFunction.Ease.EaseOutQuart:
                function = EasingFunction.GetEasingFunction(EasingFunction.Ease.EaseInQuart);
                break;
            case EasingFunction.Ease.EaseInOutQuart:
                break;
            case EasingFunction.Ease.EaseInQuint:
                function = EasingFunction.GetEasingFunction(EasingFunction.Ease.EaseOutQuint);
                break;
            case EasingFunction.Ease.EaseOutQuint:
                function = EasingFunction.GetEasingFunction(EasingFunction.Ease.EaseInQuint);
                break;
            case EasingFunction.Ease.EaseInOutQuint:
                break;
            case EasingFunction.Ease.EaseInSine:
                function = EasingFunction.GetEasingFunction(EasingFunction.Ease.EaseOutSine);
                break;
            case EasingFunction.Ease.EaseOutSine:
                function = EasingFunction.GetEasingFunction(EasingFunction.Ease.EaseInSine);
                break;
            case EasingFunction.Ease.EaseInOutSine:
                break;
            case EasingFunction.Ease.EaseInExpo:
                function = EasingFunction.GetEasingFunction(EasingFunction.Ease.EaseOutExpo);
                break;
            case EasingFunction.Ease.EaseOutExpo:
                function = EasingFunction.GetEasingFunction(EasingFunction.Ease.EaseInExpo);
                break;
            case EasingFunction.Ease.EaseInOutExpo:
                break;
            case EasingFunction.Ease.EaseInCirc:
                function = EasingFunction.GetEasingFunction(EasingFunction.Ease.EaseOutCirc);
                break;
            case EasingFunction.Ease.EaseOutCirc:
                function = EasingFunction.GetEasingFunction(EasingFunction.Ease.EaseInCirc);
                break;
            case EasingFunction.Ease.EaseInOutCirc:
                break;
            case EasingFunction.Ease.Linear:
                break;
            case EasingFunction.Ease.Spring:
                break;
            case EasingFunction.Ease.EaseInBounce:
                break;
            case EasingFunction.Ease.EaseOutBounce:
                break;
            case EasingFunction.Ease.EaseInOutBounce:
                break;
            case EasingFunction.Ease.EaseInBack:
                break;
            case EasingFunction.Ease.EaseOutBack:
                break;
            case EasingFunction.Ease.EaseInOutBack:
                break;
            case EasingFunction.Ease.EaseInElastic:
                break;
            case EasingFunction.Ease.EaseOutElastic:
                break;
            case EasingFunction.Ease.EaseInOutElastic:
                break;
            default:
                break;
        }
    }

}