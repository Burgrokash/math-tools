﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pyros
{
    public static class MatrixHandler
    {
        public static Vector2Int GetMatrixCenter(Vector2Int matrixSizes)
        {
            return new Vector2Int(matrixSizes.x / 2, matrixSizes.y / 2);
        }

        public static Vector3Int GetMatrixCenter(Vector3Int matrixSizes)
        {
            return new Vector3Int(matrixSizes.x / 2, matrixSizes.y / 2, matrixSizes.z / 2);
        }

        public static Vector2Int GetUpIndex(GridElement[,] matrix, Vector2Int coordinate)
        {
            return GetGridIndex(matrix, coordinate, Vector2Int.Up());
        }

        public static Vector2Int GetLeftIndex(GridElement[,] matrix, Vector2Int coordinate)
        {
            return GetGridIndex(matrix, coordinate, Vector2Int.Left());
        }

        public static Vector2Int GetDownIndex(GridElement[,] matrix, Vector2Int coordinate)
        {
            return GetGridIndex(matrix, coordinate, Vector2Int.Down());
        }

        public static Vector2Int GetRightIndex(GridElement[,] matrix, Vector2Int coordinate)
        {
            return GetGridIndex(matrix, coordinate, Vector2Int.Right());
        }

        public static Vector2Int GetGridIndex(GridElement[,] matrix, Vector2Int coordinate, Vector2Int direction)
        {
            Vector2Int upperCoordinate = coordinate + direction;

            if (!IndexExist(matrix, upperCoordinate))
            {
                Debug.LogWarning("The coordinate you are trying to acces do not exist in the current matrix.");
                return null;
            }
            else
                return upperCoordinate;
        }

        public static bool IndexExist(GridElement[,] matrix, Vector2Int coordinate)
        {
            if (coordinate.x >= matrix.GetLowerBound(0) && coordinate.x <= matrix.GetUpperBound(0))
            {
                if (coordinate.y >= matrix.GetLowerBound(1) && coordinate.y <= matrix.GetUpperBound(1))
                {
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

    }

}

