﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Functions
{
    public static float Lineal(float t)
    {
        return t;
    }
    public static float Cuadratic(float t)
    {
        return Mathf.Pow(t, 2);
    }
    public static float Cubic(float t)
    {
        return Mathf.Pow(t, 3);
    }

    public static float Logarithmic(float t)
    {
        return Mathf.Log(t);
    }

}
