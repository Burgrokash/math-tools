﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunctionDrawer : MonoBehaviour
{
    [SerializeField]
    private GameObject linePrefab;
    
    private GameObject currentLine;

    private LineRenderer lineRenderer;

    [SerializeField]
    private AdvanceSettings advanceSettings;

    [SerializeField]
    private Function functionToDraw;

    private void Awake()
    {
        CreateLine();
    }

    [ContextMenu("Draw")]
    public void Draw()
    {
        functionToDraw.DevelopFunction();
        DrawFunction(functionToDraw.GetPoints().ToArray());
    }

    /// <summary>
    /// Spawns a line to use for drawing a function curve.
    /// </summary>
    public void CreateLine()
    {
        currentLine = Instantiate(linePrefab, Vector3.zero, Quaternion.identity);
        currentLine.transform.SetParent(this.transform);
        lineRenderer = currentLine.GetComponent<LineRenderer>();
    }

    public void DrawFunction(Vector2[] pointsToDraw)
    {
        StartCoroutine(DrawingFunction(pointsToDraw));
    }

    IEnumerator DrawingFunction(Vector2[] points)
    {
        lineRenderer.positionCount = 0;

        for (int i = 0; i < points.Length; i++)
        {
            lineRenderer.positionCount++;

            // Hay que revisar esta conversion de 3D a 2D.
            lineRenderer.SetPosition(i, points[i]);

            yield return new WaitForSeconds(advanceSettings.step);
        }
    }

    private void OnValidate()
    {
        if (!linePrefab)
        {
            Debug.LogWarning("Please assign a prefab with a line renderer to the linePrefab field on the inspector.");
        }
    }

    [System.Serializable]
    private class AdvanceSettings
    {
        /// <summary>
        /// The distance un "time" on wich each new point is going to be drawn.
        /// </summary>
        public float step;
    }

}
