﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Function : MonoBehaviour
{
    [SerializeField]
    private FunctionTypes type = FunctionTypes.None;

    [SerializeField]
    private Vector2 functionDomain = new Vector2(-10f,10f);

    [SerializeField]
    private float sampleRate = 0.1f;

    delegate float myFunction(float t);
    myFunction currentFuncion;

    [SerializeField]
    private List<Vector2> points = new List<Vector2>();

    [ContextMenu("Valuate")]
    public void DevelopFunction()
    {
        points.Clear();

        float currentOriginPoint = functionDomain.x;

        while(!Mathf.Approximately(currentOriginPoint, functionDomain.y + sampleRate))
        {
            points.Add(new Vector2(currentOriginPoint, Mathf.Round(currentFuncion.Invoke(currentOriginPoint) * 100) / 100));
            currentOriginPoint += sampleRate;
        }
    }

    public List<Vector2> GetPoints()
    {
        return points;
    }

    public void SetFunction()
    {
        switch (type)
        {
            case FunctionTypes.Lineal:
                currentFuncion += Functions.Lineal;
                break;
            case FunctionTypes.Cuadratic:
                currentFuncion += Functions.Cuadratic;
                break;
            case FunctionTypes.Cubic:
                currentFuncion += Functions.Cubic;
                break;
            case FunctionTypes.Logarithmic:
                currentFuncion += Functions.Logarithmic;
                break;
            case FunctionTypes.None:
                break;
        }
    }

    private void OnValidate()
    {
        SetFunction();
    }

}
