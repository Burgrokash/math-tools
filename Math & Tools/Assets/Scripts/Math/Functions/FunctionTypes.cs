﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FunctionTypes
{
    None,
    Lineal,
    Cuadratic,
    Cubic,
    Logarithmic,
}
